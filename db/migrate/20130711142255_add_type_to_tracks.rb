class AddTypeToTracks < ActiveRecord::Migration
  def change
    add_column :tracks, :type, :string
  end
end
