class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  def welcome_email(user)

    @user = user
    @url = activate_users_url(token: @user.token)
    mail(to: @user.email, subject: "Welcome to music!")
  end
end
