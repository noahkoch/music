class Album < ActiveRecord::Base
  attr_accessible :band_id, :name, :recording_type

  belongs_to :band
  has_many :tracks, dependent: :destroy

  validates :name, :band_id, :recording_type , presence: true
end
