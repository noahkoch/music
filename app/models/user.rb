class User < ActiveRecord::Base
  attr_accessor :password_confirmation
  attr_accessible :admin, :email, :password, :password_confirmation, :token, :active
  has_many :notes
  validates :email, uniqueness: true, presence: true
  validates_confirmation_of :password

  before_create :make_token

  def logout
    self.token = nil
    save
  end

  def make_token
    self.token = SecureRandom.base64
  end
end
