class UsersController < ApplicationController

  def activate
    user = User.find_by_token!(params[:token])

    if user.token == params[:token]
      user.active = true
      user.save

      session[:token] = user.token

      flash[:notice] = "Account activated successfully!"
      redirect_to root_url
    else
      flash[:alert] = "Unable to verify your account."
      redirect_to root_url
    end
  end

  def index
    @users = User.all
  end

  def login
    @user = User.find_by_email(params[:email])

    if @user && (@user.password == params[:password]) && @user.active
      @user.make_token
      @user.save
      session[:token] = @user.token

      flash[:notice] = "Logged in successfully as #{@user.email}"
      redirect_to root_url
    elsif @user && !@user.active
      flash[:alert] = "Check your email for an activation code!"
      redirect_to root_url
    else
      flash[:alert] = "Wrong email/password combination"
      redirect_to root_url
    end
  end

  def logout
    current_user.logout
    session[:token] = nil

    redirect_to root_url
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])

    if @user.save
      msg = UserMailer.welcome_email(@user)
      msg.deliver!

      flash[:notice] = "Successfully created new user"
      redirect_to root_url
    else
      render :new
    end
  end

  def destroy
    @user.destroy

    flash[:notice] = "User deleted"
    redirect_to :root_path
  end
end