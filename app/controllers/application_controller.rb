class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :get_object, only: [:show, :edit, :update, :destroy]

  include ApplicationHelper

  def get_object
    model = self.class.to_s.gsub(/Controller/, '').singularize.constantize
    instance_variable_set("@#{model.to_s.downcase}", model.find(params[:id]))
  end
end
