class BandsController < ApplicationController


  def index
    @bands = Band.all
  end

  def show
  end

  def new
    @band = Band.new
  end

  def create
    @band = Band.new(params[:band])

    if @band.save
      redirect_to @band
    else
      render :new
    end
  end

  def edit

  end

  def update
  end

  def destroy
    if user_is_admin?
      @band.destroy

      flash[:notice] = "Successully deleted #{@band.name}"
      redirect_to bands_url
    else
      flash[:alert] = "You're not allowed"
      redirect_to root_url
    end
  end
end
