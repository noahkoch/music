class AlbumsController < ApplicationController
  def index
    @albums = Album.all
  end

  def show
  end

  def new
    @album = Album.new(band_id: params[:band_id])
    @bands = Band.all
  end

  def create
    @album = Album.new(params[:album])

    if @album.save
      redirect_to @album
    else
      @bands = Band.all
      render :new
    end
  end

  def edit
    @bands = Band.all
  end

  def update
  end

  def destroy
    if user_is_admin?
      @album.destroy

      flash[:notice] = "Successfully deleted #{@album.name}"
      redirect_to bands_url
    else
      flash[:alert] = "You're not allowed"
      redirect_to root_url
    end
  end

end
