class NotesController < ApplicationController
  def create
    @note = Note.new(params[:note])

    if @note.user_id != current_user.id
      flash[:alert] = "Can only create notes for currently logged in user"
      redirect_to root_url
    elsif @note.save
      flash[:notice] = "Note saved successfully"
      redirect_to @note.track
    else
      flash[:alert] = "Note not saved!"
      redirect_to @note.track
    end
  end

  def destroy
    @note.destroy

    flash[:notice] = "Successfully deleted the note"
    redirect_to @note.track
  end
end
