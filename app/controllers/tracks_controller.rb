class TracksController < ApplicationController
  def index
    @tracks = Track.all
  end

  def show
  end

  def new
    @track = Track.new(album_id: params[:album_id])
    @albums = Album.all
  end

  def create
    @track = Track.new(params[:track])

    if @track.save
      redirect_to @track
    else
      @albums = Album.all
      render :new
    end
  end

  def edit
    @albums = Album.all
  end

  def update
  end

  def destroy
    if user_is_admin?
      @track.destroy

      flash[:notice] = "Successfully deleted #{@track.name}"
      redirect_to bands_path
    else
      flash[:alert] = "You are not allowed!"
      redirect_to root_path
    end
  end

end
