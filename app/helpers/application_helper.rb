module ApplicationHelper

  def current_user
    User.find_by_token(session[:token]) if session[:token]
  end

  def logged_in?
    !!current_user
  end

  def user_is_admin?
    current_user && current_user.admin
  end

  def ugly_lyrics(lyrics)
    "<pre>#{lyrics.split("\n").map { |line| "&#9835; " + line }.join}</pre>".html_safe
  end

end
